package com.fetech.smart.validate.exception;

/**
 * 参数校验异常
 * Created by ZhangGang on 2016/9/5.
 */
public class ParamsException extends RuntimeException {

    private static final long serialVersionUID = 276486514583932180L;
    private String message;


    public ParamsException(String msg) {
        super(msg);
        this.message=msg;
    }

    /**
     * 重写fillInStackTrace方法会使得这个自定义的异常不会收集线程的整个异常栈信息，
     * 会大大提高减少异常开销。
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public String setMessage(String msg) {
        return this.message=msg;
    }
}
