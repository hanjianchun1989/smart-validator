package com.fetech.smart.validate.utils;

import com.esotericsoftware.reflectasm.MethodAccess;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 加入reflectasm，去掉beanutils
 */
public class ReflectAsmUtil {
    private static final ConcurrentMap<Class, MethodAccess> localCache = new ConcurrentHashMap<>();

    public static MethodAccess get(Class clazz) {
        if (localCache.containsKey(clazz)) {
            return localCache.get(clazz);
        }

        MethodAccess methodAccess = MethodAccess.get(clazz);
        localCache.putIfAbsent(clazz, methodAccess);
        return methodAccess;
    }

    public static  Object getFieldValue(Object target, String fieldName) {
        MethodAccess targetMethodAccess = get(target.getClass());
        try {
            Object value = targetMethodAccess.invoke(target, "get" + CommonUtil.capitalize(fieldName));
            return value;
        } catch (Exception e) {
            // 设置异常，可能会没有对应字段，忽略
        }
        return null;
    }
}